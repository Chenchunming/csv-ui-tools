#pragma once
#ifndef _CINIFILE_H_ 
#define _CINIFILE_H_ 

#undef AFX_EXT_CLASS 
#undef AFX_EXT_API 
#undef AFX_EXT_DATA 
#define AFX_EXT_CLASS AFX_CLASS_EXPORT 
#define AFX_EXT_API AFX_API_EXPORT 
#define AFX_EXT_DATA AFX_DATA_EXPORT 
//用户接口说明:在成员函数SetVarStr和SetVarInt函数中,当iType等于零,则如果用户制定的参数在ini文件中不存在, 
//则就写入新的变量.当iType不等于零,则如果用户制定的参数在ini文件中不存在,就不写入新的变量，而是直接返回FALSE; 
class AFX_EXT_CLASS CIniFile
{
public:
	CIniFile();
	virtual ~CIniFile();
private:
	CIniFile(const CIniFile &);
	CIniFile & operator = (const CIniFile &);
public:

	//创建函数 
	int Create(const CString &strFileName);
	//得到变量整数型数值 
	int GetVarInt(const CString &, const CString &, int &);
	//得到变量字符串型数值 
	int GetVarStr(const CString &, const CString &, CString &);
	//重新设置变量整数型数值 
	int SetVarInt(const CString &, const CString &, const int &, const int iType = 1);
	//重新设置变量字符串型数值 
	int SetVarStr(const CString &, const CString &, const CString &, const int iType = 1);
	//Loudy add a function 
	//遍历一个section中的key的数目 
	int GetKeyCount(const CString &);
	//读取一个Key的列表 
	int GetKeyList(CStringArray &, const CString &strSection);
	//读取Section的数目 
	int CIniFile::GetSectionCount(CStringArray & SectionArray);

private:
	int GetVar(const CString &, const CString &, CString &);
	int SetVar(const CString &, const CString &, const CString &, const int iType = 1);
	int SearchLine(const CString &, const CString &);



private:
	//	vector <CString>  FileContainer; 
	CArray <CString, CString> FileContainer;
	BOOL  bFileExsit;
	CStdioFile stfFile;
	CString strInIFileName;

};

#endif


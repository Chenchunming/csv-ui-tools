
// ParamSetDlg.h : 头文件
//

#pragma once


// CParamSetDlg 对话框
class CParamSetDlg : public CDialogEx
{
// 构造
public:
	CParamSetDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PARAMSET_DIALOG };
#endif
	int ContentList[20];
	int ShiftList[10] = { IDC_EDIT20, IDC_EDIT21, IDC_EDIT22, IDC_EDIT23, IDC_EDIT24, IDC_EDIT25, IDC_EDIT26, IDC_EDIT27, IDC_EDIT28, 0 };
	CString csvfilename;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonImport();
	afx_msg void OnBnClickedButtonExport();
	afx_msg void OnSelchangeComboShift();
	afx_msg void OnBnClickedButtonCatbin();
};

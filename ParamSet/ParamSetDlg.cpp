
// ParamSetDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ParamSet.h"
#include "ParamSetDlg.h"
#include "afxdialogex.h"
#include <locale>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CParamSetDlg 对话框



CParamSetDlg::CParamSetDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PARAMSET_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CParamSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CParamSetDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_IMPORT, &CParamSetDlg::OnBnClickedButtonImport)
	ON_BN_CLICKED(IDC_BUTTON_EXPORT, &CParamSetDlg::OnBnClickedButtonExport)
	ON_CBN_SELCHANGE(IDC_COMBO19, &CParamSetDlg::OnSelchangeComboShift)
	ON_BN_CLICKED(IDC_BUTTON_CATBIN, &CParamSetDlg::OnBnClickedButtonCatbin)
END_MESSAGE_MAP()


// CParamSetDlg 消息处理程序

BOOL CParamSetDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	CString strName, strAppName, strVars;
	int EditList[20] = { IDC_EDIT1, IDC_EDIT2, IDC_EDIT3, IDC_EDIT4, IDC_EDIT5, IDC_EDIT6, IDC_EDIT7, IDC_EDIT8, IDC_EDIT9, IDC_EDIT10,
		IDC_EDIT11, IDC_EDIT12, IDC_EDIT13, IDC_EDIT14, IDC_EDIT15, IDC_EDIT16, IDC_EDIT17, IDC_EDIT18, IDC_EDIT19, 0 };
	int ComboList[20] = { IDC_COMBO1, IDC_COMBO2, IDC_COMBO3, IDC_COMBO4, IDC_COMBO5, IDC_COMBO6, IDC_COMBO7, IDC_COMBO8, IDC_COMBO9, IDC_COMBO10,
		IDC_COMBO11, IDC_COMBO12, IDC_COMBO13, IDC_COMBO14, IDC_COMBO15, IDC_COMBO16, IDC_COMBO17, IDC_COMBO18, IDC_COMBO19, 0 };
	int StaticList[20] = { IDC_STATIC1, IDC_STATIC2, IDC_STATIC3, IDC_STATIC4, IDC_STATIC5, IDC_STATIC6, IDC_STATIC7, IDC_STATIC8, IDC_STATIC9, IDC_STATIC10,
		IDC_STATIC11, IDC_STATIC12, IDC_STATIC13, IDC_STATIC14, IDC_STATIC15, IDC_STATIC16, IDC_STATIC17, IDC_STATIC18, IDC_STATIC19, 0 };

	for (int i = 0; i < 19; i++) {
		strAppName.Format(_T("%d"), i + 1);
		GetPrivateProfileString(strAppName, _T("Name"), _T("N/A"), strName.GetBuffer(MAX_PATH), MAX_PATH, _T("./config.ini"));
		::SetWindowText(::GetDlgItem(m_hWnd, StaticList[i]), strName);
		if (GetPrivateProfileInt(strAppName, _T("Type"), 0, _T("./config.ini"))) {
			WCHAR chVars[MAX_PATH];
			GetPrivateProfileString(strAppName, _T("Vars"), _T("0"), chVars, MAX_PATH, _T("./config.ini"));
			strVars.Format(_T("%s"), chVars);

			CString strGap = _T(",");
			CString strLeft;
			int nPos = strVars.Find(strGap);
			while (nPos > 0)
			{
				strLeft = strVars.Left(nPos);
				if (!strLeft.IsEmpty())
					((CComboBox *)GetDlgItem(ComboList[i]))->InsertString(-1, strLeft);
				strVars = strVars.Right(strVars.GetLength() - nPos - 1);
				nPos = strVars.Find(strGap);
			}
			if (!strVars.IsEmpty()) {
				((CComboBox *)GetDlgItem(ComboList[i]))->InsertString(-1, strVars);
			}

			::ShowWindow(::GetDlgItem(m_hWnd, EditList[i]), SW_HIDE);
			::ShowWindow(::GetDlgItem(m_hWnd, ComboList[i]), SW_SHOW);
			ContentList[i] = ComboList[i];
		}
		else {
			::ShowWindow(::GetDlgItem(m_hWnd, EditList[i]), SW_SHOW);
			ContentList[i] = EditList[i];
			::ShowWindow(::GetDlgItem(m_hWnd, ComboList[i]), SW_HIDE);
		}
	}

	for (int i = 0; i < 9; i++) {
		GetDlgItem(ShiftList[i])->EnableWindow(FALSE);
		((CEdit*)GetDlgItem(ShiftList[i]))->SetWindowText(_T("0x0"));
	}
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CParamSetDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CParamSetDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CParamSetDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



BOOL CParamSetDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)// 屏蔽esc键  
	{
		return TRUE;// 不作任何操作  
	}
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)// 屏蔽enter键  
	{
		CWnd* pwndCtrl = GetFocus();
		// get the control ID which is presently having the focus
		int ctrl_ID = pwndCtrl->GetDlgCtrlID();
		return TRUE;// 不作任何处理  
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CParamSetDlg::OnBnClickedButtonImport()
{
	// TODO: 在此添加控件通知处理程序代码
	CString line;
	CStdioFile csvfile;
	CString buffer;
	CString comma = _T(",");
	int EditList[20] = { IDC_EDIT1, IDC_EDIT2, IDC_EDIT3, IDC_EDIT4, IDC_EDIT5, IDC_EDIT6, IDC_EDIT7, IDC_EDIT8, IDC_EDIT9, IDC_EDIT10,
		IDC_EDIT11, IDC_EDIT12, IDC_EDIT13, IDC_EDIT14, IDC_EDIT15, IDC_EDIT16, IDC_EDIT17, IDC_EDIT18, IDC_EDIT19, 0 };
	int StaticList[20] = { IDC_STATIC1, IDC_STATIC2, IDC_STATIC3, IDC_STATIC4, IDC_STATIC5, IDC_STATIC6, IDC_STATIC7, IDC_STATIC8, IDC_STATIC9, IDC_STATIC10,
		IDC_STATIC11, IDC_STATIC12, IDC_STATIC13, IDC_STATIC14, IDC_STATIC15, IDC_STATIC16, IDC_STATIC17, IDC_STATIC18, IDC_STATIC19, 0 };
	int itemcount = 0;

	CFileDialog opendlg(TRUE, _T("*"), _T("*.csv"), OFN_OVERWRITEPROMPT, _T("所有文件(*.*)|*.*||"), NULL);
	if (opendlg.DoModal() == IDOK)
	{
		csvfilename = opendlg.GetPathName();
		csvfilename.Replace(_T("\\"), _T("/"));
	}
	else
		return;

	csvfile.Open(csvfilename, CFile::modeReadWrite, NULL);
	while (csvfile.ReadString(line)) {
		CStringArray strResult;
		CString strGap = _T(",");
		int nPos = line.Find(strGap);
		CString strLeft = _T("");

		while (0 <= nPos)
		{
			strLeft = line.Left(nPos);
			if (!strLeft.IsEmpty())
				strResult.Add(strLeft);
			line = line.Right(line.GetLength() - nPos - 1);
			nPos = line.Find(strGap);
		}
		if (!line.IsEmpty()) {
			strResult.Add(line);
		}

		int nSize = strResult.GetSize();
		//if (nSize >= 3)
		//	::SetWindowText(::GetDlgItem(m_hWnd, StaticList[itemcount]), strResult.GetAt(2));
		if (nSize >= 2) {
			WCHAR chClassname[MAX_PATH];
			GetClassName(::GetDlgItem(m_hWnd, ContentList[itemcount]), chClassname, MAX_PATH);
			if (wcscmp(chClassname, _T("Edit")) == 0) {
				::SetWindowText(::GetDlgItem(m_hWnd, ContentList[itemcount]), strResult.GetAt(1));
			}
			else if (wcscmp(chClassname, _T("ComboBox")) == 0) {
				int index = _ttoi(strResult.GetAt(1));
				((CComboBox *)GetDlgItem(ContentList[itemcount]))->SetCurSel(index);
			}
		}
		itemcount++;
		if (itemcount > 18) break;
	}

	int index = 0;
	CString StrCache;
	index = ((CComboBox*)GetDlgItem(ContentList[18]))->GetCurSel();
	if (index == -1) {
		for (int i = 0; i < 9; i++)	GetDlgItem(ShiftList[i])->EnableWindow(FALSE);
	}
	else {
		if (index / 2 == 0) {
			GetDlgItem(ShiftList[0])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[1])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[2])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[3])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[4])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[5])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[6])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[7])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[8])->EnableWindow(FALSE);
		}
		else if (index / 2 == 1) {
			GetDlgItem(ShiftList[0])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[1])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[2])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[3])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[4])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[5])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[6])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[7])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[8])->EnableWindow(FALSE);
		}
		else if (index / 2 == 2) {
			GetDlgItem(ShiftList[0])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[1])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[2])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[3])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[4])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[5])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[6])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[7])->EnableWindow(FALSE);
			GetDlgItem(ShiftList[8])->EnableWindow(FALSE);
		}
		else if (index / 2 == 3) {
			GetDlgItem(ShiftList[0])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[1])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[2])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[3])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[4])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[5])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[6])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[7])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[8])->EnableWindow(TRUE);
		}
	}
	itemcount = 0;
	while (csvfile.ReadString(line)) {
		CStringArray strResult;
		CString strGap = _T(",");
		int nPos = line.Find(strGap);
		CString strLeft = _T("");

		while (0 <= nPos)
		{
			strLeft = line.Left(nPos);
			if (!strLeft.IsEmpty())
				strResult.Add(strLeft);
			line = line.Right(line.GetLength() - nPos - 1);
			nPos = line.Find(strGap);
		}
		if (!line.IsEmpty()) {
			strResult.Add(line);
		}

		int nSize = strResult.GetSize();
		if (nSize >= 2) {
			::SetWindowText(::GetDlgItem(m_hWnd, ShiftList[itemcount]), strResult.GetAt(1));
		}
		itemcount++;
	}

	//立即写入，不缓冲
	csvfile.Flush();
	//文件操作结束关闭
	csvfile.Close();
}


void CParamSetDlg::OnBnClickedButtonExport()
{
	//格式：过滤器描述符（显示作用）+ \0 + 文件扩展名称（过滤作用）  
	//多个扩展名称之间用（;）分隔，两个过滤字符串之间以\0分隔  
	//最后的过滤器需要以两个\0\0结尾  
	CStdioFile csvfile;
	CString StrCache;
	int StaticList[20] = { IDC_STATIC1, IDC_STATIC2, IDC_STATIC3, IDC_STATIC4, IDC_STATIC5, IDC_STATIC6, IDC_STATIC7, IDC_STATIC8, IDC_STATIC9, IDC_STATIC10,
		IDC_STATIC11, IDC_STATIC12, IDC_STATIC13, IDC_STATIC14, IDC_STATIC15, IDC_STATIC16, IDC_STATIC17, IDC_STATIC18, IDC_STATIC19, 0 };
	int nItem;
	CString szFilters = _T("Comma-Separated Values File(*.csv)\0*.csv\0" \
		"All Typle(*.*)\0*.*\0" \
		"\0");
	//当过滤器或者默认构造参数赋值较少情况下，  
	//使用构造函数修改对话框初始状态可能更好，这过滤器较多  
	CFileDialog savedlg(FALSE, _T("*"), _T("*.csv"), OFN_OVERWRITEPROMPT, szFilters);

	if (IDOK == savedlg.DoModal())
	{
		csvfilename = savedlg.GetPathName();
		csvfilename.Replace(_T("\\"), _T("/"));
		csvfile.Open(csvfilename, CFile::modeCreate | CFile::modeReadWrite, NULL);

		for (nItem = 0; ContentList[nItem] > 0; nItem++) {
			StrCache.Format(_T("0x%02x"), nItem);
			csvfile.WriteString(StrCache);
			csvfile.WriteString(_T(","));

			WCHAR chClassname[MAX_PATH];
			GetClassName(::GetDlgItem(m_hWnd, ContentList[nItem]), chClassname, MAX_PATH);
			if (wcscmp(chClassname, _T("Edit")) == 0) {
				((CEdit*)GetDlgItem(ContentList[nItem]))->GetWindowText(StrCache);
				if (StrCache.IsEmpty())
					StrCache.Format(_T("0x%x"), 0);
			}
			else if (wcscmp(chClassname, _T("ComboBox")) == 0) {
				int index = 0;
				index = ((CComboBox*)GetDlgItem(ContentList[nItem]))->GetCurSel();
				if(index == -1)
					StrCache.Format(_T("0x%x"), 0);
				else
					StrCache.Format(_T("0x%x"), index);
			}
			csvfile.WriteString(StrCache);
			csvfile.WriteString(_T(","));

			((CStatic*)GetDlgItem(StaticList[nItem]))->GetWindowText(StrCache);
			char* old_locale = _strdup(setlocale(LC_CTYPE, NULL));
			setlocale(LC_CTYPE, "chs");//设定
			csvfile.WriteString(StrCache);
			setlocale(LC_CTYPE, old_locale);
			free(old_locale);//还原区域设定
			csvfile.WriteString(_T("\n"));
		}

		int shiftcount = ((CComboBox*)GetDlgItem(ContentList[18]))->GetCurSel();
		if (shiftcount == -1)	shiftcount = 0;
		else {
			if (shiftcount / 2 == 0)	shiftcount = 3;
			else if (shiftcount / 2 == 1)	shiftcount = 5;
			else if (shiftcount / 2 == 2)	shiftcount = 7;
			else if (shiftcount / 2 == 3)	shiftcount = 9;
		}
		for (nItem = 0; nItem < shiftcount; nItem++) {
			StrCache.Format(_T("0x%x"), nItem + 19);
			csvfile.WriteString(StrCache);
			csvfile.WriteString(_T(","));

			((CEdit*)GetDlgItem(ShiftList[nItem]))->GetWindowText(StrCache);
			csvfile.WriteString(StrCache);
			csvfile.WriteString(_T(","));
			csvfile.WriteString(_T("\n"));
		}
		//立即写入，不缓冲  
		csvfile.Flush();
		//文件操作结束关闭  
		csvfile.Close();
	}
}


void CParamSetDlg::OnSelchangeComboShift()
{
	int  index = ((CComboBox*)GetDlgItem(ContentList[18]))->GetCurSel();
	if (index == -1) {
		for (int i = 0; i < 9; i++) {
			GetDlgItem(ShiftList[i])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[i]))->SetWindowText(_T("0x0"));
		}
	}
	else {
		if (index / 2 == 0) {
			GetDlgItem(ShiftList[0])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[1])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[2])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[3])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[3]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[4])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[4]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[5])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[5]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[6])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[6]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[7])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[7]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[8])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[8]))->SetWindowText(_T("0x0"));
		}
		else if (index / 2 == 1) {
			GetDlgItem(ShiftList[0])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[1])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[2])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[3])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[4])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[5])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[5]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[6])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[6]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[7])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[7]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[8])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[8]))->SetWindowText(_T("0x0"));
		}
		else if (index / 2 == 2) {
			GetDlgItem(ShiftList[0])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[1])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[2])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[3])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[4])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[5])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[6])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[7])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[7]))->SetWindowText(_T("0x0"));
			GetDlgItem(ShiftList[8])->EnableWindow(FALSE);
			((CEdit*)GetDlgItem(ShiftList[8]))->SetWindowText(_T("0x0"));
		}
		else if (index / 2 == 3) {
			GetDlgItem(ShiftList[0])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[1])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[2])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[3])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[4])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[5])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[6])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[7])->EnableWindow(TRUE);
			GetDlgItem(ShiftList[8])->EnableWindow(TRUE);
		}
	}
}


void CParamSetDlg::OnBnClickedButtonCatbin()
{
	CString binfilename, jointbinfilename;
	UINT confsize, appsize;
	STARTUPINFO si = { sizeof(si) };
	PROCESS_INFORMATION pi;

	if (csvfilename.IsEmpty()) {
		MessageBox(_T("No parameters to proecess!"));
		return;
	}

	CFileDialog opendlg(TRUE, _T("*"), _T("*.bin"), OFN_OVERWRITEPROMPT, _T("Application binary(*.bin)|*.bin||"), NULL);
	if (opendlg.DoModal() == IDOK)
	{
		binfilename = opendlg.GetPathName();
		binfilename.Replace(_T("\\"), _T("/"));
	}
	else
		return;

	CString strCmd = _T("component/csv2bin.exe ") + csvfilename + _T(" component/conf.bin");
	if (!CreateProcess(NULL, (LPWSTR)strCmd.GetString(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
		MessageBox(_T("Transfer csv to bin Fail!"));
		DeleteFile(_T("component/conf.bin"));
		return;
	}
	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	CFileStatus fileStatus;
	CStdioFile appbin;
	appbin.Open(binfilename, CFile::modeRead | CFile::typeBinary, NULL);
	CStdioFile confbin;
	confbin.Open(_T("component/conf.bin"), CFile::modeRead | CFile::typeBinary, NULL);
	CStdioFile outputbin;
	outputbin.Open(_T("component/output.bin"), CFile::modeCreate | CFile::modeReadWrite | CFile::typeBinary, NULL);

	if (confbin.GetStatus(fileStatus))	confsize = (UINT)fileStatus.m_size;
	if (appbin.GetStatus(fileStatus))	appsize = (UINT)fileStatus.m_size;

	UINT uRead;
	byte *buff = new byte[1024];
	byte *zeros = new byte[131072];
	memset(zeros, 0, 131072);
	do
	{
		uRead = confbin.Read(buff, 1024);
		outputbin.Write(buff, uRead);
	} while (uRead>0);
	outputbin.Write(zeros, 16384 - confsize);

	do
	{
		uRead = appbin.Read(buff, 1024);
		outputbin.Write(buff, uRead);
	} while (uRead>0);
	outputbin.Write(zeros, 131072 - appsize);

	appbin.Close();
	confbin.Close();
	outputbin.Close();

	CFileDialog savedlg(FALSE, _T("*"), _T("*.bin"), OFN_OVERWRITEPROMPT, _T("Joint binary(*.bin;)|*.bin||"));
	if (savedlg.DoModal() == IDOK) {
		jointbinfilename = savedlg.GetPathName();
		jointbinfilename.Replace(_T("\\"), _T("/"));
	}
	else {
		DeleteFile(_T("component/conf.bin"));
		DeleteFile(_T("component/output.bin"));
		return;
	}

	BOOL retcopyfile = CopyFile(_T("component/output.bin"), jointbinfilename, TRUE);
	if (!retcopyfile)
		MessageBox(_T("File exist, export error!"));
	DeleteFile(_T("component/conf.bin"));
	DeleteFile(_T("component/output.bin"));
}

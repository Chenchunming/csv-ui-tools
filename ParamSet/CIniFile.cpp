#include "stdafx.h"
#include "CIniFile.h"   
CIniFile::CIniFile() :bFileExsit(FALSE)
{
}
CIniFile::~CIniFile()
{
	if (bFileExsit)
	{
		if (stfFile.Open(strInIFileName, CFile::modeCreate | CFile::modeWrite) && FileContainer.GetSize() > 0)
		{
			CString strParam;
			for (int i = 0; i< FileContainer.GetSize(); i++)
			{
				strParam = FileContainer[i];
				//  stfFile.WriteString(strParam);   
				stfFile.WriteString(strParam + "\n");
			}
		}
		stfFile.Close();
	}
	if (FileContainer.GetSize() > 0)
	{
		FileContainer.RemoveAll();
	}
}
//生成一个ini文件，如果文件不存在就什么都不做返回false,如果文件存在就返回true,内容读入内存CArray中保存。   
BOOL  CIniFile::Create(const CString & strFileName)
{
	bFileExsit = FALSE;
	strInIFileName = strFileName;
	if (!stfFile.Open(strFileName, CFile::modeRead))
	{
		return bFileExsit;
	}
	CString strFileLine;
	while (bFileExsit = stfFile.ReadString(strFileLine))
	{
		if (bFileExsit == FALSE)
			return bFileExsit;
		FileContainer.Add(strFileLine);
	}
	stfFile.Close();
	bFileExsit = TRUE;
	return bFileExsit;
}
BOOL CIniFile::GetVar(const CString & strSection, const CString & strVarName, CString &strReturnValue)
{
	if (bFileExsit == FALSE || FileContainer.GetSize() < 0)
		return bFileExsit;

	int iLine = SearchLine(strSection, strVarName);
	if (iLine > 0)
	{

		CString     strParam = FileContainer[iLine - 1];
		strReturnValue = strParam.Mid(strParam.Find(_T("=")) + 1);
		return TRUE;
	}
	return FALSE;
}
BOOL CIniFile::GetVarStr(const CString & strSection, const CString & strVarName, CString &strReturnValue)
{
	return(GetVar(strSection, strVarName, strReturnValue));
}
BOOL CIniFile::GetVarInt(const CString & strSection, const CString & strVarName, int & iValue)
{
	CString  strReturnVar;
	if (GetVar(strSection, strVarName, strReturnVar))
	{
		strReturnVar.TrimLeft();
		int iLen = strReturnVar.GetLength();
		iValue = atoi((char*)strReturnVar.GetBuffer(iLen));
		return TRUE;
	}
	return TRUE;
}
//用户接口说明:在成员函数SetVarStr和SetVarInt函数中,当iType等于零,则如果用户制定的参数在ini文件中不存在,   
//则就写入新的变量.当iType不等于零,则如果用户制定的参数在ini文件中不存在,就不写入新的变量，而是直接返回FALSE;   
BOOL CIniFile::SetVar(const CString & strSection, const CString & strVarName, const CString & strVar, const int iType)
{
	if (bFileExsit == FALSE)
		return bFileExsit;
	if (FileContainer.GetSize() == 0)
	{
		FileContainer.Add(_T("[") + strSection + _T("]"));
		FileContainer.Add(strVarName + "=" + strVar);
		return TRUE;
	}

	int i = 0;
	int iFileLines = FileContainer.GetSize();
	//for(pInterator;pInterator != FileContainer.end();++pInterator)   
	//{   
	while (i< iFileLines)
	{

		CString     strValue = FileContainer.GetAt(i++);
		strValue.TrimLeft();
		strValue.TrimRight();
		if ((strValue.Find(_T("[")) == 0) && (strValue.Find(strSection) >= 0))
		{
			while (i < iFileLines)
			{

				CString strSectionList = FileContainer[i++];
				strSectionList.TrimLeft();
				if (strSectionList.Find(_T("//")) == 0)//找到注释行   
					continue;

				if (strSectionList.Find(strVarName) >= 0)//找到   
				{

					CString strParam = strVarName + "=" + strVar;

					//FileContainer.SetAt(i-1,strParam);   
					FileContainer[i - 1] = strParam;

					return TRUE;
				}
				if (strSectionList.Find(_T("["), 0) >= 0)//在原来文件的SECTION中,没有相应的变量需要添加而且,这种情况下,下边还有别的section   
				{
					//处理流程是这样的,首先把当前的数值依次向后移动,然后在当前位置加入新出现的数值   
					if (iType != 0)
						return FALSE;
					CString strParam;
					FileContainer.Add(strParam);
					int iPre = FileContainer.GetSize() - 1;
					while (iPre >= i)
					{
						CString strBehind = FileContainer[iPre - 1];
						FileContainer[iPre] = strBehind;
						iPre--;

					}//*/   
					strParam = strVarName + "=" + strVar;
					FileContainer.SetAt(i - 1, strParam);
					return TRUE;
				}
				if (i == iFileLines && iType == 0)
				{
					FileContainer.Add(strVarName + "=" + strVar);
					return TRUE;
				}
			}
		}
	}
	if (iType == 0)
	{
		FileContainer.Add(_T("[") + strSection + _T("]"));
		FileContainer.Add(strVarName + "=" + strVar);
	}

	return TRUE;

}
BOOL CIniFile::SetVarStr(const CString & strSection, const CString & strVarName, const CString & strValue, const int iType)
{
	return SetVar(strSection, strVarName, strValue, iType);
}
BOOL CIniFile::SetVarInt(const CString & strSection, const CString & strVarName, const int & iValue, const int iType)
{
	CString strVar;
	strVar.Format(_T("%d"), iValue);
	return (SetVar(strSection, strVarName, strVar, iType));
}
//查找Section存在VarName的定义属性吗？找到有效值就返回是在文件第几行。否则返回小于0的数。－1文件中没有找到 ，－2在section中没有找到   
int CIniFile::SearchLine(const CString & strSection, const CString & strVarName)
{

	if (FileContainer.GetSize() > 0)
	{

		int i = 0;
		int iFileLines = FileContainer.GetSize();
		while (i< iFileLines)
		{

			CString strValue = FileContainer[i++];
			strValue.TrimLeft();
			strValue.TrimRight();
			if (strValue.Find(_T("[")) == 0 && strValue.Find(strSection, 1) >= 0)  //找到[Section]   
			{
				while (i < iFileLines)
				{

					CString strSectionList = FileContainer[i++];
					strSectionList.TrimLeft();
					if (strSectionList.Find(_T("//")) == 0)//找到注释行 跳过注释 //   
						continue;
					if (strSectionList.Find(strVarName) >= 0)//找到有效值就返回在文件的行数   
					{
						return i;
					}
					strSectionList.TrimLeft();
					if (strSectionList.Find(_T("["), 0) == 0)//另外一个段落出现,寻找失败   
					{
						return -2;
					}

				}
			}
		}
	}
	return -1;
}

int CIniFile::GetKeyCount(const CString &strSection)
{
	if (FileContainer.GetSize() > 0)
	{
		unsigned int iCount = 0;
		int i = 0;
		int iFileLines = FileContainer.GetSize();
		while (i< iFileLines)
		{

			CString strValue = FileContainer[i++];
			strValue.TrimLeft();
			if (strValue.Find(_T("[")) >= 0 && strValue.Find(strSection, 1) >= 0)   //找到[Section]   
			{
				while (i < iFileLines)
				{

					CString strSectionList = FileContainer[i++];
					strSectionList.TrimLeft();
					if (strSectionList.Find(_T("//")) == 0)//找到注释行 跳过注释 //   
						continue;

					if (!(strSectionList.Find(_T("["), 0) == 0)) //另外一个段落出现,寻找失败,返回行   
					{
						iCount++;
					}
					else
					{
						return iCount;
					}

				}
			}
		}
	}
	return -1;
}


/////////////////////////   
BOOL CIniFile::GetKeyList(CStringArray &KeyStrArray, const CString &strSection)
{
	if (FileContainer.GetSize() > 0)
	{

		KeyStrArray.RemoveAll();
		int i = 0;
		int iFileLines = FileContainer.GetSize();
		while (i< iFileLines)
		{

			CString strValue = FileContainer[i++];
			strValue.TrimLeft();
			strValue.TrimRight();
			if (strValue.Find(_T("[")) == 0 && strValue.Find(strSection, 1) >= 0)  //找到[Section]   
			{
				while (i < iFileLines)
				{
					CString strSectionList = FileContainer[i++];
					strSectionList.TrimLeft();
					if (strSectionList.Find(_T("//")) == 0)//找到注释行 跳过注释 //   
						continue;
					strSectionList.TrimLeft();
					if (!(strSectionList.Find(_T("["), 0) == 0)) //另外一个段落出现,寻找失败,返回行   
					{
						KeyStrArray.Add(strSectionList.Mid(0, strSectionList.Find(_T("="))));
					}
					else
					{
						return true;
					}

				}
			}
		}
	}
	return false;
}
/////////////////////////////////////////////////////////////////////   
int CIniFile::GetSectionCount(CStringArray & SectionArray)
{
	int iSecCount = 0;
	if (FileContainer.GetSize() > 0)
	{
		SectionArray.RemoveAll();
		int i = 0;
		int iFileLines = FileContainer.GetSize();
		while (i< iFileLines)
		{

			CString strValue = FileContainer[i++];
			strValue.TrimLeft();
			strValue.TrimRight();
			if (strValue.Find(_T("[")) == 0 && strValue.Find(_T("]")) != 1)    //找到[Section]   
			{
				iSecCount++;
				SectionArray.Add(strValue.Mid(0, strValue.Find(_T("]")) - 1));
			}
		}
	}
	return iSecCount;
}